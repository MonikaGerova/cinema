import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { MovieComponent } from './movie/movie.component';
import { ProjectionComponent } from './projection/projection.component';
import { PaymentComponent } from './payment/payment.component';

const appRoutes: Routes= [
  {path: '', component: AppComponent},
  {path:"projection/:id", component:ProjectionComponent},
  {path:"projection/:id/payment", component:PaymentComponent},
  ];

@NgModule({
  declarations: [
    AppComponent,
    MovieComponent,
    ProjectionComponent,
    PaymentComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(appRoutes,{useHash: true})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
