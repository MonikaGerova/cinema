import { Component, OnInit, Input } from '@angular/core';
import { ProjectionService, ProjectionPreview } from '../projection/projection.service';

export enum DaysOfWeek { 
 Su = 0,
 Mo = 1,
 Tu = 2,
 We = 3,
 Th = 4,
 Fr = 5,
 Sa = 6
}

@Component({
  selector: 'movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {
  @Input() id: string;
  @Input() name: string;

  projections: ProjectionPreview[];
  week: DaysOfWeek[];

  constructor( private projectionService: ProjectionService) {
    this.projections = [];
    this.week =  this.orderDays();
  }

  ngOnInit() {
    this.projectionService.getPreview(this.id).subscribe(data=>{
      this.projections = data;  
    }); 
  }

  orderDays() { 
    let weekDays = [];

    const today = new Date().getDay();
    let day = today;
    while(weekDays.length < 7) {
      if(day > 6) {
        day = 0;
      }
      weekDays.push(DaysOfWeek[day]);
      day = day + 1;
    }

    return weekDays;
  }

  filterByDay(day: string){
    if(this.projections.filter) {
      const filtered = this.projections.filter(d =>DaysOfWeek[d.day]== day );
      return filtered;
    }
  }
}
