import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { map } from "rxjs/operators";

export interface MovieData{
  id: number;
  name: string;
}

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private HTTP: Http) {}

  getMovieName(movieId:string) {
    return this.HTTP.post('../../src/api/Movie.php',{"id":movieId}).pipe(map(res=>res.json()));
  }

  getMovies(){
    return this.HTTP.post('../../src/api/Movie.php',null).pipe(map(res=>{
      let movies = res.json();
      if(movies.map) {
        movies = movies.map(d => {
          const movie:  MovieData = {
            id: d.Id,
            name: d.Name
          };
          
          return movie;
        });
      }
      return movies;
    }));
  }
}
