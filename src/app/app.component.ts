import { Component } from '@angular/core';
import { MovieService, MovieData } from './Movie/movie.service';
import { AuditoryService } from './auditory/auditory.service';
import { ProjectionService } from './projection/projection.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  movies: MovieData[];
  title = 'Cinema';

  constructor(private movieSerice: MovieService) {
    // this.movieSerice.getMovieName('1').subscribe(data=>{
    //   console.log(data);
    // });

    // this.auditoryService.getOccupancyPlan('1').subscribe(data=>{
    //   console.log(data);
    // });

    // const date= "2018-09-23 10:00:00";
    // this.projectionService.getProjectionDetails('1',date).subscribe(data=>{
    //   console.log(data);
    // });

    // this.projectionService.getPreview('1').subscribe(data=>{
    //   console.log(data);
    // }); 

    this.movieSerice.getMovies().subscribe(data=>this.movies = data);
  }
}
