import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectionService, ProjectionDetails } from './projection.service';
import { AuditoryService } from '../auditory/auditory.service';

@Component({
  selector: 'projection',
  templateUrl: './projection.component.html',
  styleUrls: ['./projection.component.css']
})
export class ProjectionComponent implements OnInit {
  @ViewChild('projection') el;

  id: number;
  projectionData: ProjectionDetails;
  auditoryPlan: string[][];

  ticketsCount: number = 0;

  constructor( 
    private projectionService: ProjectionService,
    private auditoryService: AuditoryService,
    private route: ActivatedRoute,
    private router: Router
    
  ) { 
    this.projectionData = {
      movieName: null,
      auditoryId: null,
      date: null,
      price: null
    };
    
    this.auditoryPlan = [];
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    this.projectionService.getProjectionDetails(this.id).subscribe(data => {
        this.projectionData = data;
        this.getAuditoryPlan(this.projectionData.auditoryId);
    });
  }

  getAuditoryPlan(id: string) {
    this.auditoryService.getOccupancyPlan(id).subscribe(data=>{
      this.auditoryPlan = data;
    });
  }

  //methods which check the validity of the seat
  isSeat(seat:string): boolean {
    return seat === "X" || seat==="O" || seat === 'Y'; 
  }
  
  isOccupied(seat: string): boolean {
    return seat === 'O';
  }

  occupy(event: any, i: number,j:number): void {
    console.log(i,j);
    if(event.target.checked) {
      this.ticketsCount = this.ticketsCount +1;
      
      this.auditoryPlan[i][j] = 'Y';

      console.log(this.auditoryPlan[i][j])
    } else {
      this.ticketsCount = this.ticketsCount -1; 
      this.auditoryPlan[i][j] = 'X';
    }
  }

  goBack(): void {
    this.router.navigate(['']);
  }
}
