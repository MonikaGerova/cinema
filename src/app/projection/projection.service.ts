import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { map } from "rxjs/operators";

export interface ProjectionPreview{
  day: number;
  time: string;
  date: number;
  projectionId: number;
}

export interface ProjectionDetails {
  movieName: string;
  auditoryId: string;
  price: string;
  date: Date;
}

@Injectable({
  providedIn: 'root'
})
export class ProjectionService {
  constructor(private HTTP: Http) { }

  getProjectionDetails(projectionId:number) {
    return this.HTTP.post('../../src/api/Projection.php',{"projection_id": projectionId}).pipe(map(res=>{
      let projection = res.json();
      if(projection.map) {
        projection = projection.map(p => {
          const projectionInfo: ProjectionDetails =  {
            date: new Date(p.date),
            auditoryId: p.auditoryId,
            price: p.price,
            movieName: p.movieName
          }

          return projectionInfo;
        });
      }

      return projection[0];
    }));
  }

  getPreview(movieId: string){
    return this.HTTP.post('../../src/api/Projection.php',{"movie_id":movieId}).pipe(map(res=>{
      let projections = res.json();
      if(projections.map) {
        projections = projections.map(p => {
          const date = new Date(p.Date);
          const projectionDate: ProjectionPreview = {
            day: date.getDay(),
            time: `${date.getHours()}:${date.getMinutes() || '00'}`,
            date: date.getDate(),
            projectionId: p.ProjectionId
          }

          return projectionDate;
        });
      }
      return projections;
    }));
  }
}
