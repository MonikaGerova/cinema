import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuditoryService {

  constructor(private HTTP: Http) { }

  getOccupancyPlan(auditoryId:string) {
    return this.HTTP.post('../../src/api/Auditory.php',{"id":auditoryId}).pipe(map(res=> {
      let plan = res.json();
      return plan.split('\n').map(e => e.split(""));
    }));
  }
}
