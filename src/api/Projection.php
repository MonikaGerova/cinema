<?php 
    include "CRUD.php";
    $crud= new Crud();

    $input=json_decode(file_get_contents("php://input"));
    if(sizeOf($input)!=0){
        if(property_exists($input,'projection_id')){
            $projection_id = $input->projection_id;
            
            $query="SELECT name as movieName,auditoryId,price,date From projections JOIN movies ON MovieId = Id where ProjectionId='$projection_id'";
        } else if(property_exists($input,'movie_id')){
            $movie_id=$input->movie_id;

            $query="SELECT Date, ProjectionId From projections JOIN movies ON MovieId = Id where MovieId='$movie_id' 
               AND Date between DATE(CURDATE()) AND DATE_ADD(DATE(CURDATE()), INTERVAL 7 DAY)"; 
        }
       
        $data=$crud->getData($query);

        if(sizeOf($data)== 0) {
            echo json_encode('{}');
        } else {
            echo json_encode($data);
        }
           
    } else {
        echo json_encode('{}');
    }
?>