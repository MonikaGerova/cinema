-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 22, 2018 at 10:04 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cinema`
--

-- --------------------------------------------------------

--
-- Table structure for table `auditories`
--

CREATE TABLE `auditories` (
  `id` int(11) NOT NULL,
  `occupancyPlan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auditories`
--

INSERT INTO `auditories` (`id`, `occupancyPlan`) VALUES
(1, 'Auditories/1.txt');

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE `movies` (
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`Name`, `Id`) VALUES
('Titanic', 1),
('Matrix', 2),
('Harry Potter', 3);

-- --------------------------------------------------------

--
-- Table structure for table `projections`
--

CREATE TABLE `projections` (
  `ProjectionId` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  `AuditoryId` int(11) NOT NULL,
  `MovieId` int(11) NOT NULL,
  `Price` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projections`
--

INSERT INTO `projections` (`ProjectionId`, `Date`, `AuditoryId`, `MovieId`, `Price`) VALUES
(1, '2018-09-23 10:00:00', 1, 1, '7');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auditories`
--
ALTER TABLE `auditories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `projections`
--
ALTER TABLE `projections`
  ADD PRIMARY KEY (`ProjectionId`),
  ADD KEY `Auditory foreign key` (`AuditoryId`),
  ADD KEY `Movie foreign key` (`MovieId`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `projections`
--
ALTER TABLE `projections`
  ADD CONSTRAINT `Auditory foreign key` FOREIGN KEY (`AuditoryId`) REFERENCES `auditories` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `Movie foreign key` FOREIGN KEY (`MovieId`) REFERENCES `movies` (`Id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
